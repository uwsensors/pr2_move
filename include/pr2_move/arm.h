#ifndef ARM_H
#define ARM_H

#include <ros/ros.h>
#include <pr2_controllers_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <moveit/move_group_interface/move_group.h>
#include <tf/transform_listener.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
#include <moveit/kinematic_constraints/utils.h>
#include <math.h>

typedef actionlib::SimpleActionClient<pr2_controllers_msgs::JointTrajectoryAction> TrajClient;

class Arm{
private:
    TrajClient* trajClient;
	 moveit::planning_interface::MoveGroup* group;
     tf::TransformListener tfListener;
     std::string side;
public:
    Arm(const std::string& side);
    ~Arm();
    bool jointPose(const std::vector<float>& positions);
    bool filter_roll(moveit_msgs::RobotTrajectory* traj);
    bool waypointPose(const std::vector<geometry_msgs::PoseStamped>& poses,
                                            double velScale,
                                            bool async,
                                            bool avoidCollisions,
																						moveit_msgs::Constraints pathConstraints,
                                            double eef_step=0.01);
    moveit_msgs::RobotTrajectory scaleTrajectorySpeed(moveit_msgs::RobotTrajectory* traj, double scale);
	geometry_msgs::PoseStamped getCurrentPose();

    static const double VEL_TOLERANCE = 0.0001;
    static bool armIsMoving(const std::string& side);
};
#endif
