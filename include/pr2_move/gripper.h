#ifndef GRIPPER_H
#define GRIPPER_H

#include <ros/ros.h>
#include <pr2_controllers_msgs/Pr2GripperCommandAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<pr2_controllers_msgs::Pr2GripperCommandAction> GripperClient;

class Gripper{

private:
    GripperClient* gripperClient;

public:
    Gripper(const std::string& side);
    ~Gripper();
    void open();
    void close();
    void set(double position, double effort=-1.0, bool async=false);
	void stop();

};

#endif

