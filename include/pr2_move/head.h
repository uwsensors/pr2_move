#ifndef HEAD_H
#define HEAD_H

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <pr2_controllers_msgs/PointHeadAction.h>

typedef actionlib::SimpleActionClient<pr2_controllers_msgs::PointHeadAction> PointHeadClient;

class Head{

private:
    PointHeadClient* pointHeadClient;

public:
    Head();
    ~Head();
    void pointHead(double x, double y, double z,
                   const std::string& targetFrame,
                   const std::string& referenceFrame);
};
#endif
