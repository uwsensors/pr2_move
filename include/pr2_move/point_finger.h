#ifndef POINT_FINGER_H
#define POINT_FINGER_H

#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include "pr2_move/PointFingerAction.h"

typedef actionlib::SimpleActionClient<pr2_move::PointFingerAction> PointFingerClient;

class PointFinger {

private:
    PointFingerClient* point_finger_client_;

public:
    PointFinger(const std::string& side);
    ~PointFinger();
    void point_finger(double x, double y, double z,
                      const std::string& target_frame,
                      const std::string& reference_frame,
                      const ros::Duration& min_duration,
                      bool async);
};

#endif
