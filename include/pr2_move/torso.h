#ifndef TORSO_H
#define TORSO_H
#include <ros/ros.h>
#include <pr2_controllers_msgs/SingleJointPositionAction.h>
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<pr2_controllers_msgs::SingleJointPositionAction> TorsoClient;

class Torso{
private:
  TorsoClient *torsoClient;

public:
  Torso();
  ~Torso();
  void toHeight(double height);
};


#endif
