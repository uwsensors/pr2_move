#include <pr2_move/arm.h>

Arm::Arm(const std::string& side) {
    // Need to start an AsyncSpinner, currently done in armMoveService
    std::string controller;
    if(side.compare("left")==0) {
        controller = "l_arm_controller/joint_trajectory_action";
        this->side = "left";
    }else{
        controller = "r_arm_controller/joint_trajectory_action";
        this->side = "right";
    }
    trajClient = new TrajClient(controller, true);
    group = new moveit::planning_interface::MoveGroup(this->side+"_arm");

    while(!trajClient->waitForServer(ros::Duration(5.0))) {
        ROS_INFO("Waiting for the joint_trajectory_action server");
    }

}

Arm::~Arm() {
    delete trajClient;
    delete group;
}

bool Arm::jointPose(const std::vector<float>& positions) {

    pr2_controllers_msgs::JointTrajectoryGoal goal;

    goal.trajectory.joint_names.resize(7);
    if(this->side.compare("left")==0) {
        goal.trajectory.joint_names[0] = "l_upper_arm_roll_joint";
        goal.trajectory.joint_names[1] = "l_shoulder_pan_joint";
        goal.trajectory.joint_names[2] = "l_shoulder_lift_joint";
        goal.trajectory.joint_names[3] = "l_forearm_roll_joint";
        goal.trajectory.joint_names[4] = "l_elbow_flex_joint";
        goal.trajectory.joint_names[5] = "l_wrist_flex_joint";
        goal.trajectory.joint_names[6] = "l_wrist_roll_joint";

    } else {
        goal.trajectory.joint_names[0] = "r_upper_arm_roll_joint";
        goal.trajectory.joint_names[1] = "r_shoulder_pan_joint";
        goal.trajectory.joint_names[2] = "r_shoulder_lift_joint";
        goal.trajectory.joint_names[3] = "r_forearm_roll_joint";
        goal.trajectory.joint_names[4] = "r_elbow_flex_joint";
        goal.trajectory.joint_names[5] = "r_wrist_flex_joint";
        goal.trajectory.joint_names[6] = "r_wrist_roll_joint";

    }

    goal.trajectory.points.resize(1);

    goal.trajectory.points[0].positions.resize(7);
    goal.trajectory.points[0].velocities.resize(7);
    for(int i = 0; i < 7; i++) {
        goal.trajectory.points[0].positions[i] = positions[i];
        goal.trajectory.points[0].velocities[i] = 0.0;

    }

    goal.trajectory.points[0].time_from_start = ros::Duration(1.0);

    goal.trajectory.header.stamp = ros::Time::now();
    trajClient->sendGoal(goal);
    bool result = trajClient->waitForResult();
    return result;

}

bool Arm::filter_roll(moveit_msgs::RobotTrajectory* traj) {
    int forearm_roll_idx = -1;
    int wrist_roll_idx = -1;

    // Find forearm and wrist roll indices
    for(unsigned int i = 0; i < traj->joint_trajectory.joint_names.size(); i++) {
        if(traj->joint_trajectory.joint_names[i].find("_forearm_roll_joint") != std::string::npos) {
            forearm_roll_idx = i;
        } else if(traj->joint_trajectory.joint_names[i].find("_wrist_roll_joint") != std::string::npos) {
            wrist_roll_idx = i;
        }
    }

    if(forearm_roll_idx < 0 || wrist_roll_idx < 0) {
        return false;
    }

    // First make sure everything between 0 and 2*pi
    for(unsigned int i = 0; i < traj->joint_trajectory.points.size(); i++) {
        while(traj->joint_trajectory.points[i].positions[forearm_roll_idx] < 0.0) {
            traj->joint_trajectory.points[i].positions[forearm_roll_idx] += 2*M_PI;
        }
        while(traj->joint_trajectory.points[i].positions[forearm_roll_idx] > 2*M_PI) {
            traj->joint_trajectory.points[i].positions[forearm_roll_idx] -= 2*M_PI;
        }

        while(traj->joint_trajectory.points[i].positions[wrist_roll_idx] < 0.0) {
            traj->joint_trajectory.points[i].positions[wrist_roll_idx] += 2*M_PI;
        }
        while(traj->joint_trajectory.points[i].positions[wrist_roll_idx] > 2*M_PI) {
            traj->joint_trajectory.points[i].positions[wrist_roll_idx] -= 2*M_PI;
        }
    }


    return true;

}

bool Arm::waypointPose(const std::vector<geometry_msgs::PoseStamped>& poses,
                                             double velScale,
                                             bool async,
                                             bool avoidCollisions,
																						 moveit_msgs::Constraints pathConstraints,
                                             double eef_step) {
    std::vector<geometry_msgs::Pose> wayPoints(0);
    ros::Time now = ros::Time::now();
    geometry_msgs::PoseStamped tmpPose;
    tfListener.waitForTransform(poses[0].header.frame_id, group->getPlanningFrame(),now, ros::Duration(1.0));
    for(int i = 0; i < poses.size(); i++) {
        geometry_msgs::PoseStamped pose_in(poses[i]);
        pose_in.header.stamp = now;

        try{
            tfListener.transformPose(group->getPlanningFrame(), pose_in, tmpPose);
        } catch(tf::TransformException ex) {
            ROS_ERROR("%s", ex.what());
            return false;
        }
        wayPoints.push_back(tmpPose.pose);
    }

		bool useConstraints = false;
		moveit_msgs::Constraints goalConstraints;
		if(pathConstraints.position_constraints.size() > 0) {
			useConstraints = true;
			goalConstraints.name = pathConstraints.name;
			goalConstraints.position_constraints = pathConstraints.position_constraints;
			
			geometry_msgs::PoseStamped poseIn;
			poseIn.header.stamp = now;
			for(unsigned int i = 0; i < goalConstraints.position_constraints.size(); i++) {
				poseIn.header.frame_id = goalConstraints.position_constraints[i].header.frame_id;
				for(unsigned int j = 0; j < goalConstraints.position_constraints[i].constraint_region.primitive_poses.size(); j++) {
					poseIn.pose = goalConstraints.position_constraints[i].constraint_region.primitive_poses[j];
					try{
						tfListener.transformPose(group->getPlanningFrame(), poseIn, tmpPose);
					} catch(tf::TransformException& ex) {
						ROS_ERROR("Couldn't transform from %s to %s: %s", poseIn.header.frame_id.c_str(), group->getPlanningFrame().c_str(), ex.what());
						return false;
					}
					goalConstraints.position_constraints[i].constraint_region.primitive_poses[j] = tmpPose.pose;
				}
				for(unsigned int j = 0; j < goalConstraints.position_constraints[i].constraint_region.mesh_poses.size(); j++) {
					poseIn.pose = goalConstraints.position_constraints[i].constraint_region.mesh_poses[j];
					try{
						tfListener.transformPose(group->getPlanningFrame(), poseIn, tmpPose);
					} catch(tf::TransformException& ex) {
						ROS_ERROR("Couldn't transform from %s to %s: %s", poseIn.header.frame_id.c_str(), group->getPlanningFrame().c_str(), ex.what());
						return false;
					}
					goalConstraints.position_constraints[i].constraint_region.mesh_poses[j] = tmpPose.pose;
				}

				goalConstraints.position_constraints[i].header.frame_id = group->getPlanningFrame();
			}
			
		}

		if(pathConstraints.joint_constraints.size() > 0 || 
			 pathConstraints.orientation_constraints.size() > 0 ||
			 pathConstraints.visibility_constraints.size() > 0) {
			ROS_WARN("Only position constraints are supported, all other constraints will not be applied");
		}

    moveit_msgs::RobotTrajectory* traj = new moveit_msgs::RobotTrajectory();
    group->clearPoseTargets();
		group->clearPathConstraints();
    
		// Try to compute a path
    double success = 0.0;
		
    while(success < 0.95 && eef_step < 1.0) {
				if(useConstraints) {
                    //success = group->computeCartesianPath(wayPoints, eef_step, 0, *traj, goalConstraints, avoidCollisions);
				} else {
					success = group->computeCartesianPath(wayPoints, eef_step, 0, *traj, avoidCollisions);
				}
        
        eef_step = 1.5*eef_step;
    }

    if(success < 0.95) {
        ROS_ERROR("Couldn't compute trajectory: success = %f", success);
        return false;
    } else {
        ROS_INFO("Found trajectory: success = %f", success);
    }

    if(!avoidCollisions) {
        filter_roll(traj);
    }

    // Compute velocities for found path
    if(velScale < 1.0) {
        trajectory_processing::IterativeParabolicTimeParameterization tsHelp;
        robot_trajectory::RobotTrajectory armParams(group->getCurrentState()->getRobotModel(),side+"_arm");
        armParams.setRobotTrajectoryMsg(*(group->getCurrentState()), *traj);
        bool tsComp = tsHelp.computeTimeStamps(armParams);
        tsHelp.computeTimeStamps(armParams);
        armParams.getRobotTrajectoryMsg(*traj);
        Arm::scaleTrajectorySpeed(traj, velScale);
    }

    moveit::planning_interface::MoveItErrorCode code = moveit_msgs::MoveItErrorCodes::FAILURE;
    moveit::planning_interface::MoveGroup::Plan plan;

    plan.trajectory_ = *traj;
    if(async) {
        code = group->asyncExecute(plan);
    } else {
        code = group->execute(plan);
    }

    if(code != moveit_msgs::MoveItErrorCodes::SUCCESS) {
        ROS_ERROR("Could not execute trajectory");
        return false;
    }

    return true;

}

bool Arm::armIsMoving(const std::string& side) {
    sensor_msgs::JointStateConstPtr jointState = ros::topic::waitForMessage<sensor_msgs::JointState>("joint_states");
    bool moving = false;
    int startIdx = 31; // Index for left upper_arm_roll
    if(side.compare("left") != 0) {
        startIdx = 17; // Index for right_upper_arm_roll
    }
    for(int i = startIdx; i < startIdx+7; i++) {
        if(std::abs(jointState->velocity[i]) > VEL_TOLERANCE) {
            moving = true;
        }
    }
    return moving;
}

geometry_msgs::PoseStamped Arm::getCurrentPose() {
    std::string sideBegin(1,side[0]);
    return group->getCurrentPose(sideBegin+"_wrist_roll_link");
}

// Scale the velocity of a trajectory
// Derived from code by Patrick Goebel (https://groups.google.com/forum/#!topic/moveit-users/7n45S8DUjys)
moveit_msgs::RobotTrajectory Arm::scaleTrajectorySpeed(moveit_msgs::RobotTrajectory* traj, double scale) {
	if(scale >= 1.0) {
		return *traj;
	}

	int nJoints = traj->joint_trajectory.joint_names.size();
	int nPoints = traj->joint_trajectory.points.size();
    std::vector<trajectory_msgs::JointTrajectoryPoint>* points = &traj->joint_trajectory.points;

	for(int i = 0; i < nPoints; i++) {
		(*points)[i].time_from_start *= (1.0/scale);

		for(int j = 0; j < nJoints; j++) {
			(*points)[i].velocities[j] *= scale;
			(*points)[i].accelerations[j] *= (scale*scale);
		}
	}
	
	return *traj;

}
