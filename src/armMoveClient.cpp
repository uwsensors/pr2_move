#include <ros/ros.h>
#include <pr2_move/ArmMovePoses.h>

int main(int argc, char** argv) {
	ros::init(argc, argv, "move_arm_client");
	if (argc != 2){
        ROS_INFO("Error: needs one argument: side");
        return -1;
    }

	ros::NodeHandle n;

	ros::ServiceClient armClient = n.serviceClient<pr2_move::ArmMovePoses>("move_arm_poses");

	pr2_move::ArmMovePoses ArmSrv;
        //rArmSrv.request.side = "left";
	//rArmSrv.request.side = "right";
	ArmSrv.request.side = argv[1];
	ArmSrv.request.velScale = 1.0;
	ArmSrv.request.async = false;
    ArmSrv.request.avoidCollisions = false;
	std::string arg((argv[1])); //construct a string object
	
	geometry_msgs::PoseStamped pose;
	//set the header accordingly
	if(arg.compare("left")==0){
		pose.header.frame_id = "l_wrist_roll_link";
	}else if(arg.compare("right")==0){
		pose.header.frame_id = "r_wrist_roll_link";
	}else{ //error case
        ROS_INFO("Error: needs a correct argument: 'left'  or 'right'");
        return -1;
	}
	pose.header.stamp = ros::Time(0);
	pose.pose.position.x = 0.075;
	pose.pose.position.y = 0.0;
	pose.pose.position.z = 0.0;
	pose.pose.orientation.x = 0.0;
	pose.pose.orientation.y = 0.0;
	pose.pose.orientation.z = 0.0;
	pose.pose.orientation.w = 1.0;
	ArmSrv.request.poses.push_back(pose);
	while(ros::ok()) {
		ros::service::waitForService("move_arm_poses");
		armClient.call(ArmSrv);
		ArmSrv.request.poses[0].pose.position.x *= -1.0;
	}

    return 0;

}
