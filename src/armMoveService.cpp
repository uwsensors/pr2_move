#include <pr2_move/arm.h>
#include <pr2_move/ArmMoveJoints.h>
#include <pr2_move/ArmMovePoses.h>
#include <pr2_move/ArmCurPose.h>

Arm* rArmPtr;
Arm* lArmPtr;

bool moveArmJoints(pr2_move::ArmMoveJoints::Request &req, pr2_move::ArmMoveJoints::Response &res) {

    bool result = false;
    if(req.side.compare("left")==0) {
        result = lArmPtr->jointPose(req.positions);
    } else {
        rArmPtr->jointPose(req.positions);
    }

    return true;
}

bool moveArmPoses(pr2_move::ArmMovePoses::Request &req, pr2_move::ArmMovePoses::Response &res) {
    if(req.eefStep < 10e-8) {
        req.eefStep = 0.01;
    }

    bool success = false;
    if(req.side.compare("left")==0) {
        success = lArmPtr->waypointPose(req.poses, req.velScale, req.async, req.avoidCollisions, req.pathConstraints, req.eefStep);
        res.curPose = lArmPtr->getCurrentPose();
    } else {
        success = rArmPtr->waypointPose(req.poses, req.velScale, req.async, req.avoidCollisions, req.pathConstraints, req.eefStep);
        res.curPose = rArmPtr->getCurrentPose();
    }


    return success;
}

bool moveCurPose(pr2_move::ArmCurPose::Request &req, pr2_move::ArmCurPose::Response &res) {
    if(req.side.compare("left")==0) {
        res.curPose = lArmPtr->getCurrentPose();
    } else {
        res.curPose = rArmPtr->getCurrentPose();
    }
    return true;
}

int main(int argc, char** argv) {
  
    ros::init(argc, argv, "move_arm_service");
    ros::NodeHandle n;

    // Wait for MoveIt to start
    ROS_INFO("ARM MOVE SERVICE WAITING 10 S FOR MOVEIT TO START");
    ros::Duration(10.0).sleep();
    ROS_INFO("GOING TO START ARM MOVE SERVICE");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    rArmPtr = new Arm("right");
    lArmPtr = new Arm("left");

    ros::ServiceServer jointService = n.advertiseService("move_arm_joints", moveArmJoints);
    ros::ServiceServer posesService = n.advertiseService("move_arm_poses", moveArmPoses);
    ros::ServiceServer curPoseService = n.advertiseService("move_cur_pose", moveCurPose);
    //  ros::spin();
    while(ros::ok());
    return 0;

}
