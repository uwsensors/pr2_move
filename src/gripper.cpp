#include <pr2_move/gripper.h>

Gripper::Gripper(const std::string& side) {
  std::string controller;
  if(side.compare("left")==0) {
    controller = "l_gripper_controller/gripper_action";
  } else {
    controller = "r_gripper_controller/gripper_action";
  }
  gripperClient = new GripperClient(controller, true);
  while(!gripperClient->waitForServer(ros::Duration(5.0))) {
    ROS_INFO("Waiting for the gripper action server to come up");
  }
}

Gripper::~Gripper() {
  delete gripperClient;
}

void Gripper::set(double position, double effort, bool async) {
  pr2_controllers_msgs::Pr2GripperCommandGoal goal;
  position = std::max(0.0, std::min(0.087,position));
  goal.command.position = position;
  goal.command.max_effort = effort;

  gripperClient->sendGoal(goal);
  if(!async) {
    gripperClient->waitForResult(ros::Duration(8.0));
  }
}

void Gripper::open() {
  Gripper::set(0.087, -1.0);
}

void Gripper::close() {
  Gripper::set(0.0, 50.0);
}

// Gripper stopper
void Gripper::stop() {
  //gripperClient->stopTrackingGoal();	
  gripperClient->cancelGoal();
  //gripperClient->stopTrackingGoal();
  //gripperClient->getResult();
  //Gripper::set(0.087,-1.0);
}
