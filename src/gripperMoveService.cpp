#include <pr2_move/gripper.h>
#include <pr2_move/GripperMove.h>
#include <pr2_move/SimpleGripperMove.h>

Gripper *gripperLeft;
Gripper *gripperRight;
//gripperLeft = new Gripper("left");
//gripperRight = new Gripper("right");


bool moveGripper(pr2_move::GripperMove::Request &req, pr2_move::GripperMove::Response &res) {

  std::string side ("right");
  if (side.compare(req.side) == 0)
  {
  	gripperRight->set(req.position, req.effort, req.async);
  }
  else
  {
	gripperLeft->set(req.position, req.effort, req.async);
  }
  return true;

}

bool openGripper(pr2_move::SimpleGripperMove::Request &req, pr2_move::SimpleGripperMove::Response &res) {

  std::string side ("right");
  if (side.compare(req.side) == 0)
  {     
        gripperRight->open();
  }
  else
  {
        gripperLeft->open();
  }

  return true;

}

bool closeGripper(pr2_move::SimpleGripperMove::Request &req, pr2_move::SimpleGripperMove::Response &res) {

  std::string side ("right");
  if (side.compare(req.side) == 0)
  {     
        gripperRight->close();
  }
  else
  {
        gripperLeft->close();
  }
 
  return true;
}

// To stop gripper
bool stopGripper(pr2_move::SimpleGripperMove::Request &req, pr2_move::SimpleGripperMove::Response &res) {
  std::string side ("right");
  if (side.compare(req.side) == 0)
  {     
        gripperRight->stop();
  }
  else
  {
        gripperLeft->stop();
  }

  return true;
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "move_gripper_service");
  ros::NodeHandle n;
  gripperLeft = new Gripper("left");
  gripperRight = new Gripper("right");
 

  ros::ServiceServer setService = n.advertiseService("move_gripper", moveGripper);
  ros::ServiceServer openService = n.advertiseService("open_gripper", openGripper);
  ros::ServiceServer closeService = n.advertiseService("close_gripper", closeGripper);
  ros::ServiceServer stopService = n.advertiseService("stop_gripper", stopGripper);
  ros::spin();

  return 0;
}
