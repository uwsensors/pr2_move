#include <pr2_move/head.h>

Head::Head() {
  pointHeadClient = new PointHeadClient("/head_traj_controller/point_head_action", true);
  while(!pointHeadClient->waitForServer(ros::Duration(5.0))) {
    ROS_INFO("Waiting for the point_head_action server to come up");
  }
}
Head::~Head() {
  delete pointHeadClient;
}

void Head::pointHead(double x, double y, double z,
                     const std::string& targetFrame,
                     const std::string& referenceFrame) {
  pr2_controllers_msgs::PointHeadGoal goal;
  geometry_msgs::PointStamped point;
  point.header.frame_id = referenceFrame;
  point.point.x = x;
  point.point.y = y;
  point.point.z = z;
  goal.target = point;

  goal.pointing_frame = targetFrame;
  goal.min_duration = ros::Duration(0.5);
  goal.max_velocity = 1.0;

  pointHeadClient->sendGoal(goal);
  pointHeadClient->waitForResult(ros::Duration(2.0));
}

