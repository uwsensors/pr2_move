#include <pr2_move/head.h>
#include <pr2_move/HeadMove.h>

static const std::string DEFAULT_TARGET_FRAME = "head_mount_kinect_rgb_link";
static const std::string DEFAULT_REFERENCE_FRAME = "base_link";

bool moveHead(pr2_move::HeadMove::Request &req, pr2_move::HeadMove::Response &res) {

  Head head;
	std::string targetFrame = req.targetFrame.empty() ? DEFAULT_TARGET_FRAME : req.targetFrame;
	std::string refFrame = req.referenceFrame.empty() ? DEFAULT_REFERENCE_FRAME : req.referenceFrame;

	head.pointHead(req.x, req.y, req.z, targetFrame, refFrame);

  return true;
}

int main(int argc, char** argv) {

  ros::init(argc, argv, "move_head_service");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("move_head", moveHead);
  ros::spin();

  return 0;

}
