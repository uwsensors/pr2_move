#include <ros/ros.h>
#include <pr2_move/HeadMove.h>

static const std::string DEFAULT_REFERENCE_FRAME = "r_wrist_roll_link";
static const std::string DEFAULT_TARGET_FRAME = "head_mount_kinect_depth_link";

int main(int argc, char** argv) {
  ros::init(argc, argv, "head_track");
  ros::NodeHandle node;

  ros::ServiceClient headClient = node.serviceClient<pr2_move::HeadMove>("move_head");

	std::string referenceFrame;
	if(argc > 1) {
		referenceFrame = argv[1];
	} else {
		referenceFrame = DEFAULT_REFERENCE_FRAME;
	}

	std::string targetFrame;
	if(argc > 2) {
		targetFrame = argv[2];
	} else {
		targetFrame = DEFAULT_TARGET_FRAME;
	}

	double sleep;
	if(argc > 3) {
		sleep = atof(argv[3]);
	} else {
		sleep = 0.1;
	} 

	ros::Rate r((int)(1/sleep));

  pr2_move::HeadMove headSrv;
  headSrv.request.x = 0.0;
  headSrv.request.y = 0.0;
  headSrv.request.z = 0.0;
  headSrv.request.targetFrame = targetFrame;
  headSrv.request.referenceFrame = referenceFrame;
  ros::service::waitForService("move_head", ros::Duration(1.0));
	std::string sideBegin(1,referenceFrame[0]);
	ROS_INFO("Use 'rosrun pr2_controller_manager pr2_controller_manager stop %s_arm_controller' to move the robot's arm freely",sideBegin.c_str());

	while(ros::ok()) {
		headClient.call(headSrv);
		r.sleep();
	}

  return 0;
}
