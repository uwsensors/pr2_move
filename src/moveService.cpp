#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <pr2_move/HeadMove.h>
#include <pr2_move/TorsoMove.h>
#include <pr2_move/ArmMoveJoints.h>

ros::NodeHandle* nodePtr;

bool moveSleep(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
  ros::ServiceClient headClient = nodePtr->serviceClient<pr2_move::HeadMove>("move_head");
  ros::ServiceClient torsoClient = nodePtr->serviceClient<pr2_move::TorsoMove>("move_torso");

  pr2_move::HeadMove headSrv;
  headSrv.request.x = 1.0;
  headSrv.request.y = 0.0;
  headSrv.request.z = 3.0;
  headSrv.request.targetFrame = "";
  headSrv.request.referenceFrame = "";
  ros::service::waitForService("move_head", ros::Duration(1.0));
  headClient.call(headSrv);

  pr2_move::TorsoMove torsoSrv;
  torsoSrv.request.goal = 0.0;
  ros::service::waitForService("move_torso", ros::Duration(1.0));
  torsoClient.call(torsoSrv);

  return true;
}

bool moveClearArms(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res) {
  ros::ServiceClient armClient = nodePtr->serviceClient<pr2_move::ArmMoveJoints>("move_arm_joints");

  pr2_move::ArmMoveJoints lArmSrv;
  lArmSrv.request.side = "left";
  lArmSrv.request.positions.resize(7);
  lArmSrv.request.positions[0] = 0.0;
  lArmSrv.request.positions[1] = 0.8;
  lArmSrv.request.positions[2] = -0.5;
  lArmSrv.request.positions[3] = 0.0;
  lArmSrv.request.positions[4] = -1.5707;
  lArmSrv.request.positions[5] = -0.1;
  lArmSrv.request.positions[6] = 0.0;
  ros::service::waitForService("move_arm_joints", ros::Duration(1.0));
  armClient.call(lArmSrv);

  pr2_move::ArmMoveJoints rArmSrv;
  rArmSrv.request.side = "right";
  rArmSrv.request.positions.resize(7);
  rArmSrv.request.positions[0] = 0.0;
  rArmSrv.request.positions[1] = -0.8;
  rArmSrv.request.positions[2] = -0.5;
  rArmSrv.request.positions[3] = 0.0;
  rArmSrv.request.positions[4] = -1.5707;
  rArmSrv.request.positions[5] = -0.1;
  rArmSrv.request.positions[6] = 0.0;
  ros::service::waitForService("move_arm_joints", ros::Duration(1.0));
  armClient.call(rArmSrv);

  return true;
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "move_service");
  ros::NodeHandle node;
  nodePtr = &node;

  ros::ServiceServer sleepService = node.advertiseService("move_sleep", moveSleep);
  ros::ServiceServer clearArmsService = node.advertiseService("move_clear_arms", moveClearArms);
  ros::spin();

  return 0;
}
