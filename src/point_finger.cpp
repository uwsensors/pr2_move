#include "pr2_move/point_finger.h"

PointFinger::PointFinger(const std::string &side) {
    assert(side[0] == 'l' || side[0] == 'r');
    std::string action_name("/"+std::string(1,side[0])+"_arm_controller/point_finger_action");
    point_finger_client_ = new PointFingerClient(action_name, true);
    while(!point_finger_client_->waitForServer(ros::Duration(5.0))) {
        ROS_INFO("Waiting for the %s server to come up", action_name.c_str());
    }
}

PointFinger::~PointFinger() {
    delete point_finger_client_;
}

void PointFinger::point_finger(double x, double y, double z,
                               const std::string &target_frame,
                               const std::string &reference_frame,
                               const ros::Duration& min_duration,
                               bool async) {
    pr2_move::PointFingerGoal goal;
    geometry_msgs::PointStamped point;
    point.header.frame_id = reference_frame;
    point.header.stamp = ros::Time::now();
    point.point.x = x;
    point.point.y = y;
    point.point.z = z;

    geometry_msgs::Vector3 pointing_axis;
    pointing_axis.x = 1.0;
    pointing_axis.y = 0.0;
    pointing_axis.z = 0.0;

    goal.target = point;
    goal.pointing_frame = target_frame;
    goal.min_duration = min_duration;
    goal.max_velocity = 0.0;
    goal.pointing_axis = pointing_axis;

    ROS_INFO("Sending goal");
    point_finger_client_->sendGoal(goal);

    if(!async) {
        ROS_INFO("Waiting for result");
        point_finger_client_->waitForResult();
        ROS_INFO("Done waiting for result");
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "point_finger_client_test");
    ros::NodeHandle node;

    std::string side("right");
    PointFinger pf(side);

    std::string target_frame("r_gripper_r_finger_tip_link");
    std::string reference_frame("base_link");
    double x = 2.0;
    double y = -0.205;
    double z = 15.0;
    while(ros::ok()) {

        pf.point_finger(x,y,z, target_frame, reference_frame, ros::Duration(5), false);
        ros::spinOnce();
        z = -z;
    }
}
