/* Derived from pr2_point_frame in pr2_head_action package */
#include <ros/ros.h>

#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/thread/condition.hpp>

#include <actionlib/server/action_server.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Twist.h>

#include <kdl/chainfksolver.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames.hpp>
#include "kdl/chainfksolverpos_recursive.hpp"
#include "kdl_parser/kdl_parser.hpp"

#include "tf_conversions/tf_kdl.h"
#include <message_filters/subscriber.h>
#include <tf/message_filter.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/JointState.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int32.h>

#include <urdf/model.h>

#include <trajectory_msgs/JointTrajectory.h>
#include "pr2_move/PointFingerAction.h"
#include <pr2_controllers_msgs/QueryTrajectoryState.h>
#include <pr2_controllers_msgs/JointTrajectoryControllerState.h>

void printVector3(const char * label, tf::Vector3 v){
    printf("%s % 7.3f % 7.3f % 7.3f\n", label, v.x(), v.y(), v.z() );

}

void printSegment(const KDL::Segment& seg) {

    std::cout << "Name: " << seg.getName() << std::endl;
    std::cout << "Joint name: " << seg.getJoint().getName() << std::endl;
    std::cout << "Joint type: " << seg.getJoint().getTypeName() << std::endl;
    std::cout << "Origin: (" << seg.getFrameToTip().p.x() << ","
              << seg.getFrameToTip().p.y() << ","
              << seg.getFrameToTip().p.z() << ")" << std::endl;
    double roll,pitch,yaw;
    seg.getFrameToTip().M.GetRPY(roll,pitch,yaw);
    std::cout << "Rotation: (" << roll << ","
              << pitch << ","
              << yaw << ")" << std::endl;

}

class PointFinger {

private:
    typedef actionlib::ActionServer<pr2_move::PointFingerAction> PFA;
    typedef PFA::GoalHandle GoalHandle;

    std::string node_name_;
    std::string action_name_;
    std::string root_;
    std::string tip_;
    std::string forearm_link_;
    std::string default_pointing_frame_;
    std::string pointing_frame_;
    tf::Vector3 pointing_axis_;
    std::vector<std::string> joint_names_;
    std::vector<int> joint_controller_idx_;

    ros::NodeHandle nh_, pnh_;
    ros::Publisher pub_controller_command_;
    ros::Subscriber sub_controller_state_;
    ros::Subscriber command_sub_;
    ros::ServiceClient cli_query_traj_;
    ros::Timer watchdog_timer_;

    PFA action_server_;
    bool has_active_goal_;
    GoalHandle active_goal_;
    tf::Stamped<tf::Point> target_in_forearm_;
    std::string forearm_parent_;
    double success_angle_threshold_;
    double abort_angle_threshold_;

    geometry_msgs::PointStamped target_;
    KDL::Tree tree_;
    KDL::Chain chain_;
    tf::Point target_in_root_;

    boost::scoped_ptr<KDL::ChainFkSolverPos> pose_solver_;
    boost::scoped_ptr<KDL::ChainJntToJacSolver> jac_solver_;

    tf::TransformListener tfl_;
    urdf::Model urdf_model_;

public:
    PointFinger(const ros::NodeHandle& node) :
        node_name_(ros::this_node::getName()),
        action_name_("point_finger_action"), // Name of the action- 'x_arm_controller' will get prepended
        nh_(node),
        pnh_("~"),
        action_server_(nh_, action_name_.c_str(),
                       boost::bind(&PointFinger::goalCB, this, _1),
                       boost::bind(&PointFinger::cancelCB, this, _1), false),
        has_active_goal_(false) {

        std::string ns(ros::this_node::getNamespace());

        while(ns[0] == '/') {
            ns.erase(0,1);
        }

        assert(ns[0] == 'l' || ns[0] == 'r');
        std::string side(1, ns[0]);
        pnh_.param("forearm_link", forearm_link_, std::string(side+"_forearm_roll_link"));
        pnh_.param("default_pointing_frame", default_pointing_frame_, std::string(side+"_gripper_r_finger_tip_link"));
        pnh_.param("success_angle_threshold", success_angle_threshold_, 0.1);
        pnh_.param("abort_angle_threshold", abort_angle_threshold_, 0.3);

        if(forearm_link_[0] == '/') forearm_link_.erase(0,1);
        if(default_pointing_frame_[0] == '/') default_pointing_frame_.erase(0,1);

        pub_controller_command_ = nh_.advertise<trajectory_msgs::JointTrajectory>("command", 2);
        sub_controller_state_ = nh_.subscribe("state", 1, &PointFinger::controllerStateCB, this);
        cli_query_traj_ = nh_.serviceClient<pr2_controllers_msgs::QueryTrajectoryState>("query_state");

        if(tree_.getNrOfJoints() == 0) {
            std::string robot_desc_string;
            nh_.param("/robot_description", robot_desc_string, std::string());
            ROS_DEBUG("Reading tree from robot description...");
            if (!kdl_parser::treeFromString(robot_desc_string, tree_)){
                ROS_ERROR("Failed to construct kdl tree");
                exit(-1);
            }
            if (!urdf_model_.initString(robot_desc_string)){
                ROS_ERROR("Failed to parse urdf string for urdf::Model.");
                exit(-2);
            }
        }

        ROS_DEBUG("Tree has %d joints and %d segments.", tree_.getNrOfJoints(), tree_.getNrOfSegments());

        action_server_.start();

        watchdog_timer_ = nh_.createTimer(ros::Duration(1.0), &PointFinger::watchdog, this); // WDT to make sure controller is alive
    }

    void goalCB(GoalHandle gh) {
        // Before we do anything, we need to know that name of the pan_link's parent, which we will treat as the root.
        if (root_.empty()) {
            for (int i = 0; i < 10; ++i){
                try {
                    tfl_.getParent(forearm_link_, ros::Time(), root_);
                    break;
                } catch (const tf::TransformException &ex) {}
                ros::Duration(0.5).sleep();
            }
            if (root_.empty()) {
                ROS_ERROR("Could not get parent of %s in the TF tree", forearm_link_.c_str());
                gh.setRejected();
                return;
            }
        }
        if(root_[0] == '/') root_.erase(0, 1);

        ROS_DEBUG("Got point head goal!");

        // Process pointing frame and axis
        const geometry_msgs::PointStamped &target = gh.getGoal()->target;
        pointing_frame_ = gh.getGoal()->pointing_frame;

        // Get pointing_frame and pointing_axis
        if(pointing_frame_.length() == 0) {
            ROS_WARN("Pointing frame not specified, using %s [1, 0, 0] by default.", default_pointing_frame_.c_str());
            pointing_frame_ = default_pointing_frame_;
            pointing_axis_ = tf::Vector3(1.0, 0.0, 0.0);
        } else {
            if(pointing_frame_[0] == '/') pointing_frame_.erase(0, 1);
            bool ret1 = false;
            try {
                std::string error_msg;
                ret1 = tfl_.waitForTransform(forearm_link_, pointing_frame_, target.header.stamp,
                                             ros::Duration(5.0), ros::Duration(0.01), &error_msg);

                tf::vector3MsgToTF(gh.getGoal()->pointing_axis, pointing_axis_);
                if(pointing_axis_.length() < 0.1)
                {
                    ROS_WARN("Pointing axis appears to be zero-length. Using [1, 0, 0] as default.");
                    pointing_axis_ = tf::Vector3(1, 0, 0);
                }
                else
                {
                    pointing_axis_.normalize();
                }
            } catch(const tf::TransformException &ex) {
                ROS_ERROR("Transform failure (%d): %s", ret1, ex.what());
                gh.setRejected();
                return;
            }
        }

        //Put the target point in the root frame (usually torso_lift_link).
        bool ret1 = false;
        try {
            std::string error_msg;
            ret1 = tfl_.waitForTransform(root_.c_str(), target.header.frame_id, target.header.stamp,
                                         ros::Duration(5.0), ros::Duration(0.01), &error_msg);

            geometry_msgs::PointStamped target_in_root_msg;
            tfl_.transformPoint(root_.c_str(), target, target_in_root_msg );
            tf::pointMsgToTF(target_in_root_msg.point, target_in_root_);
        } catch(const tf::TransformException &ex) {
            ROS_ERROR("Transform failure (%d): %s", ret1, ex.what());
            gh.setRejected();
            return;
        }

        pr2_controllers_msgs::QueryTrajectoryState traj_state;
        traj_state.request.time = ros::Time::now() + ros::Duration(0.01);
        if (!cli_query_traj_.call(traj_state)) {
            ROS_ERROR("Service call to query controller trajectory failed.");
            gh.setRejected();
            return;
        }

        if( tip_.compare(pointing_frame_) != 0 ) {
            bool success = tree_.getChain(root_.c_str(), pointing_frame_.c_str(), chain_); // Get chain of links from root to the pointing frame
            if( !success ) {
                ROS_ERROR("Couldn't create chain from %s to %s.", root_.c_str(), pointing_frame_.c_str());
                gh.setRejected();
                return;
            }

            KDL::Chain tmp_chain;
            // Fix the chain and setup controller_joint_idx_
            for(int i = 0; i < chain_.segments.size(); i++) {
                if(chain_.segments[i].getJoint().getType() == KDL::Joint::None) {
                    tmp_chain.addSegment(chain_.segments[i]);
                } else {
                    int joint_index = -1;
                    for(int j = 0; j < traj_state.response.name.size(); j++) {
                        if(chain_.segments[i].getJoint().getName().compare(traj_state.response.name[j]) == 0) {
                            joint_index = j;
                            break;
                        }
                    }
                    if(joint_index >= 0) {
                        tmp_chain.addSegment(chain_.segments[i]);
                        joint_controller_idx_.push_back(joint_index);
                    } else {
                        KDL::Segment tmp_segment(chain_.segments[i].getName(),
                                                 KDL::Joint(chain_.segments[i].getJoint().getName(), KDL::Joint::None),
                                                 chain_.segments[i].getFrameToTip(),
                                                 chain_.segments[i].getInertia());
                        tmp_chain.addSegment(tmp_segment);
                    }
                }
            }
            chain_ = tmp_chain;
            tip_ = pointing_frame_;

            pose_solver_.reset(new KDL::ChainFkSolverPos_recursive(chain_)); // Computes forward kinematics
            jac_solver_.reset(new KDL::ChainJntToJacSolver(chain_)); // Computes Jacobian
            joint_names_.resize(chain_.getNrOfJoints());
        }

        unsigned int joints = chain_.getNrOfJoints();

        //    int segments = chain_.getNrOfSegments();
        //    ROS_INFO("Parsed urdf from %s to %s and found %d joints and %d segments.", root_.c_str(), pointing_frame_.c_str(), joints, segments);
        //    for(int i = 0; i < segments; i++)
        //    {
        //      KDL::Segment segment = chain_.getSegment(i);
        //      ROS_INFO("Segment %d, %s: joint %s type %s",
        //               i, segment.getName().c_str(), segment.getJoint().getName().c_str(), segment.getJoint().getTypeName().c_str() );
        //    }

        KDL::JntArray jnt_pos(joints), jnt_eff(joints);
        KDL::Jacobian jacobian(joints);

        /*
      std::cout << "Traj state joints: " << std::endl;
      for(int i = 0; i < traj_state.response.name.size(); i++) {
          std::cout << "\t" << traj_state.response.name[i] << std::endl;
      }
      std::cout << std::endl;

      for(int i = 0; i < chain_.segments.size(); i++) {
          std::cout << "Segment " << i << "\n";
          printSegment(chain_.segments[i]);
      }
      ROS_INFO("# of joints: %d", joints); */
        //if(traj_state.response.name.size() != joints)
        //{
        //  ROS_ERROR("Number of joints mismatch: urdf chain vs. trajectory controller state.");
        //  gh.setRejected();
        //  return;
        //}
        std::vector<urdf::JointLimits> limits_(joints);

        // Get initial joint positions and joint limits.
        for(unsigned int i = 0; i < joints; i++) {
            joint_names_[i] = traj_state.response.name[joint_controller_idx_[i]];
            limits_[i] = *(urdf_model_.joints_[joint_names_[i].c_str()]->limits);
            //ROS_DEBUG("Joint %d %s: %f, limits: %f %f", i, traj_state.response.name[i].c_str(), traj_state.response.position[i], limits_[i].lower, limits_[i].upper);
            //jnt_pos(i) = traj_state.response.position[i];
            jnt_pos(i) = 0.5*(limits_[i].upper+limits_[i].lower);
        }
        int count = 0;
        int limit_flips = 0;
        float correction_angle = 2*M_PI;
        float correction_delta = 2*M_PI;
        while( ros::ok() && fabs(correction_delta) > 0.001) {
            //get the pose and jacobian for the current joint positions
            KDL::Frame pose;
            pose_solver_->JntToCart(jnt_pos, pose);
            jac_solver_->JntToJac(jnt_pos, jacobian);

            tf::Transform frame_in_root;
            tf::poseKDLToTF(pose, frame_in_root);

            tf::Vector3 axis_in_frame = pointing_axis_.normalized();
            tf::Vector3 target_from_frame = (target_in_root_ - frame_in_root.getOrigin()).normalized();
            tf::Vector3 current_in_frame = frame_in_root.getBasis().inverse()*target_from_frame;
            float prev_correction = correction_angle;
            correction_angle = current_in_frame.angle(axis_in_frame);
            correction_delta = correction_angle - prev_correction;
            ROS_DEBUG("At step %d, joint poses are %.4f and %.4f, angle error is %f", count, jnt_pos(0), jnt_pos(1), correction_angle);
            if(correction_angle < 0.5*success_angle_threshold_) break;
            tf::Vector3 correction_axis = frame_in_root.getBasis()*(axis_in_frame.cross(current_in_frame).normalized());
            //printVector3("correction_axis in root:", correction_axis);
            tf::Transform correction_tf(tf::Quaternion(correction_axis, 0.5*correction_angle), tf::Vector3(0,0,0));
            KDL::Frame correction_kdl;
            tf::transformTFToKDL(correction_tf, correction_kdl);

            // We apply a "wrench" proportional to the desired correction
            KDL::Frame identity_kdl;
            KDL::Twist twist = diff(correction_kdl, identity_kdl);
            KDL::Wrench wrench_desi;
            for (unsigned int i=0; i<6; i++)
                wrench_desi(i) = -1.0*twist(i);

            // Converts the "wrench" into "joint corrections" with a jacbobian-transpose
            for (unsigned int i = 0; i < joints; i++){
                jnt_eff(i) = 0;
                for (unsigned int j=0; j<6; j++)
                    jnt_eff(i) += (jacobian(j,i) * wrench_desi(j));
                jnt_pos(i) += jnt_eff(i);
            }

            // Enforce limits, fix flex if stuck
            if((jnt_pos(1) < limits_[1].lower && limit_flips == 0) ||
                    (jnt_pos(1) > limits_[1].upper && limit_flips == 0)) {
                jnt_pos(1) = 0.5*(limits_[1].lower+limits_[1].upper);
                jnt_pos(0) += M_PI;
                limit_flips++;
            }

            //if(jnt_pos(0) < limits_[0].lower && limit_flips++ == 0){ jnt_pos(0) += 1.5*M_PI; }
            //if(jnt_pos(0) > limits_[0].upper && limit_flips++ == 0){ jnt_pos(0) -= 1.5*M_PI; }

            //jnt_pos(1) = std::max(limits_[1].lower, jnt_pos(1));
            //jnt_pos(1) = std::min(limits_[1].upper, jnt_pos(1));

            //for(int i = 0; i < joints; i++) {
            //    jnt_pos(i) = std::max(limits_[i].lower, jnt_pos(i));
            //    jnt_pos(i) = std::min(limits_[i].upper, jnt_pos(i));
            //}

            /*std::cout << "jnt_pos: ";
            for(int i = 0; i < joints; i++) {
                std::cout << jnt_pos(i) << ", ";
            }
            std::cout << std::endl;*/

            count++;

            if(limit_flips > 1){
                ROS_ERROR("Goal is out of joint limits, trying to point there anyway... \n");
                break;
            }
        }
        ROS_DEBUG("Iterative solver took %d steps", count);

        std::vector<double> q_goal(joints);

        for(unsigned int i = 0; i < joints; i++) {
            //jnt_pos(i) = std::max(limits_[i].lower, jnt_pos(i));
            //jnt_pos(i) = std::min(limits_[i].upper, jnt_pos(i));
            q_goal[i] = jnt_pos(i);
            //ROS_DEBUG("Joint %d %s: %f", i, joint_names_[i].c_str(), jnt_pos(i));
        }
        /*std::cout << "q_goal: ";
        for(int i = 0; i < q_goal.size(); i++) {
            std::cout << q_goal[i] << ", ";
        }
        std::cout << std::endl;*/

        if (has_active_goal_){
            active_goal_.setCanceled();
            has_active_goal_ = false;
        }

        gh.setAccepted();
        active_goal_ = gh;
        has_active_goal_ = true;


        // Computes the duration of the movement.
        ros::Duration min_duration(0.01);

        if (gh.getGoal()->min_duration > min_duration)
            min_duration = gh.getGoal()->min_duration;

        // Determines if we need to increase the duration of the movement in order to enforce a maximum velocity.
        if (gh.getGoal()->max_velocity > 0){
            // Very approximate velocity limiting.
            double dist = 0.0;
            for(int i = 0; i < joints; i++) {
                dist += pow(q_goal[i]-traj_state.response.position[joint_controller_idx_[i]],2);
            }
            dist = sqrt(dist);
            ros::Duration limit_from_velocity(dist / gh.getGoal()->max_velocity);
            if (limit_from_velocity > min_duration)
                min_duration = limit_from_velocity;
        }


        // Computes the command to send to the trajectory controller.
        trajectory_msgs::JointTrajectory traj;
        traj.header.stamp = traj_state.request.time;

        traj.joint_names = traj_state.response.name;

        traj.points.resize(2);
        traj.points[0].positions = traj_state.response.position;
        traj.points[0].velocities = traj_state.response.velocity;
        traj.points[0].time_from_start = ros::Duration(0.0);

        traj.points[1].positions = traj_state.response.position; // Start with previous joint
        for(int i = 0; i < joints; i++) {
            traj.points[1].positions[joint_controller_idx_[i]] = q_goal[i]; // Update joints
        }
        traj.points[1].velocities = std::vector<double>(traj_state.response.velocity.size(), 0.0);

        traj.points[1].time_from_start = ros::Duration(min_duration);

        pub_controller_command_.publish(traj);
    }

    void watchdog(const ros::TimerEvent &e) {
        ros::Time now = ros::Time::now();

        // Aborts the active goal if the controller does not appear to be active.
        if (has_active_goal_) {
            bool should_abort = false;
            if (!last_controller_state_) {
                should_abort = true;
                ROS_WARN("Aborting goal because we have never heard a controller state message.");
            } else if ((now - last_controller_state_->header.stamp) > ros::Duration(5.0)) {
                should_abort = true;
                ROS_WARN("Aborting goal because we haven't heard from the controller in %.3lf seconds",
                         (now - last_controller_state_->header.stamp).toSec());
            }

            if (should_abort) {
                // Stops the controller.
                trajectory_msgs::JointTrajectory empty;
                empty.joint_names = joint_names_;
                pub_controller_command_.publish(empty);

                // Marks the current goal as aborted.
                active_goal_.setAborted();
                has_active_goal_ = false;
            }
        }
    }

    void cancelCB(GoalHandle gh) {
        if (active_goal_ == gh){
            // Stops the controller.
            trajectory_msgs::JointTrajectory empty;
            empty.joint_names = joint_names_;
            pub_controller_command_.publish(empty);

            // Marks the current goal as canceled.
            active_goal_.setCanceled();
            has_active_goal_ = false;
        }
    }

    pr2_controllers_msgs::JointTrajectoryControllerStateConstPtr last_controller_state_;
    void controllerStateCB(const pr2_controllers_msgs::JointTrajectoryControllerStateConstPtr &msg) {
        static double  prev_pointing_angle_error = 0.0;

        last_controller_state_ = msg;
        ros::Time now = ros::Time::now();

        if (!has_active_goal_) {
            return;
        }

        try {
            KDL::JntArray jnt_pos(joint_controller_idx_.size());
            for(unsigned int i = 0; i < joint_controller_idx_.size(); i++) {
                jnt_pos(i) = msg->actual.positions[joint_controller_idx_[i]];


            }

            KDL::Frame pose;
            pose_solver_->JntToCart(jnt_pos, pose);

            tf::Transform frame_in_root;
            tf::poseKDLToTF(pose, frame_in_root);

            tf::Vector3 axis_in_frame = pointing_axis_.normalized();
            tf::Vector3 target_from_frame = target_in_root_ - frame_in_root.getOrigin();

            target_from_frame.normalize();
            tf::Vector3 current_in_frame = frame_in_root.getBasis().inverse()*target_from_frame;

            pr2_move::PointFingerFeedback feedback;


            feedback.pointing_angle_error = current_in_frame.angle(axis_in_frame);
            active_goal_.publishFeedback(feedback);

            //ROS_INFO("Error: %f", feedback.pointing_angle_error);
            //ROS_INFO("Thresh: %f", success_angle_threshold_);
            //ROS_INFO("Progress: %f", fabs(prev_pointing_angle_error-feedback.pointing_angle_error));
            if (feedback.pointing_angle_error < success_angle_threshold_) {
                ROS_INFO("Goal has succeeeded");
                active_goal_.setSucceeded();
                has_active_goal_ = false;
            } else if(feedback.pointing_angle_error < abort_angle_threshold_ &&
                      fabs(prev_pointing_angle_error-feedback.pointing_angle_error) < 10e-5) {
                bool moving = false;
                //std::cout << "velocities: ";
                for(int i = 0; i < msg->actual.velocities.size(); i++) {
                    //std::cout << msg->actual.velocities[i] << ", ";
                    if(fabs(msg->actual.velocities[i]) > 10e-3) {
                        moving = true;
                        break;
                    }
                }
                //std::cout << std::endl;

                if(!moving) {
                    ROS_WARN("No longer making progress towards goal, aborting");
                    active_goal_.setAborted();
                    has_active_goal_ = false;
                }

            }
            prev_pointing_angle_error = feedback.pointing_angle_error;

        } catch(const tf::TransformException &ex) {
            ROS_ERROR("Could not transform: %s", ex.what());
        }
    }


};

int main(int argc, char** argv) {
    ros::init(argc, argv, "point_finger");
    ros::NodeHandle node;
    PointFinger pf(node);
    ros::spin();
    return 0;
}
