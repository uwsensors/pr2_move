#include <ros/ros.h>
#include "pr2_move/arm.h"
#include "pr2_move/TorsoMove.h"
#include "pr2_move/HeadMove.h"
#include "pr2_move/GripperMove.h"
#include "pr2_move/ArmMoveJoints.h"

void str_to_vec(const std::string str_val, std::vector<double>& vec) {

    std::stringstream ss(str_val);
    vec.clear();

    double val;
    while(ss >> val) {
        vec.push_back(val);
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "pose_robot_node");
    ros::NodeHandle node;

    pr2_move::TorsoMove torso_srv;
    torso_srv.request.goal = 0.0;
    torso_srv.request.goal = node.param("/poseRobot/torso_height", torso_srv.request.goal);
    bool move_torso = false;
    move_torso = node.param("/poseRobot/move_torso", move_torso);

    pr2_move::HeadMove head_srv;
    head_srv.request.referenceFrame = "";
    head_srv.request.targetFrame = "";
    head_srv.request.x = 1.0;
    head_srv.request.y = 0.0;
    head_srv.request.z = 3.0;
    head_srv.request.referenceFrame = node.param("/poseRobot/reference_frame",
                                                 head_srv.request.referenceFrame);
    head_srv.request.targetFrame = node.param("/poseRobot/target_frame",
                                              head_srv.request.targetFrame);
    head_srv.request.x = node.param("/poseRobot/head_x", head_srv.request.x);
    head_srv.request.y = node.param("/poseRobot/head_y", head_srv.request.y);
    head_srv.request.z = node.param("/poseRobot/head_z", head_srv.request.z);
    bool move_head = false;
    move_head = node.param("/poseRobot/move_head", move_head);

    pr2_move::GripperMove l_gripper_srv;
    l_gripper_srv.request.side = "left";
    l_gripper_srv.request.async = true;
    l_gripper_srv.request.effort = -1.0;
    l_gripper_srv.request.position = 0.087;
    l_gripper_srv.request.position = node.param("/poseRobot/l_grip_width", l_gripper_srv.request.position);
    bool move_l_gripper = false;
    move_l_gripper = node.param("/poseRobot/move_l_gripper", move_l_gripper);

    pr2_move::GripperMove r_gripper_srv;
    r_gripper_srv.request.side = "right";
    r_gripper_srv.request.async = true;
    r_gripper_srv.request.effort = -1.0;
    r_gripper_srv.request.position = 0.087;
    r_gripper_srv.request.position = node.param("/poseRobot/r_grip_width", r_gripper_srv.request.position);
    bool move_r_gripper = false;
    move_r_gripper = node.param("/poseRobot/move_r_gripper", move_r_gripper);

    pr2_move::ArmMoveJoints l_arm_srv;
    l_arm_srv.request.side = "left";
    l_arm_srv.request.positions.resize(7);
    l_arm_srv.request.positions[0] = 0.0;
    l_arm_srv.request.positions[1] = 0.8;
    l_arm_srv.request.positions[2] = -0.5;
    l_arm_srv.request.positions[3] = 0.0;
    l_arm_srv.request.positions[4] = -1.5707;
    l_arm_srv.request.positions[5] = -0.1;
    l_arm_srv.request.positions[6] = 0.0;
    std::string l_joints("");
    l_joints = node.param("/poseRobot/l_joints", l_joints);
    if(l_joints.compare("") != 0) {
        std::vector<double> l_joint_vals;
        str_to_vec(l_joints, l_joint_vals);
        if(l_joint_vals.size() == 7) {
            for(int i = 0; i < 7; i++) {
                l_arm_srv.request.positions[i] = l_joint_vals[i];
            }
        } else {
            ROS_ERROR("/poseRobot/l_joints must have seven values: %s", l_joints.c_str());
            return 1;
        }
    }
    bool move_l_arm = false;
    move_l_arm = node.param("/poseRobot/move_l_arm", move_l_arm);

    pr2_move::ArmMoveJoints r_arm_srv;
    r_arm_srv.request.side = "right";
    r_arm_srv.request.positions.resize(7);
    r_arm_srv.request.positions[0] = 0.0;
    r_arm_srv.request.positions[1] = -0.8;
    r_arm_srv.request.positions[2] = -0.5;
    r_arm_srv.request.positions[3] = 0.0;
    r_arm_srv.request.positions[4] = -1.5707;
    r_arm_srv.request.positions[5] = -0.1;
    r_arm_srv.request.positions[6] = 0.0;
    std::string r_joints("");
    r_joints = node.param("/poseRobot/r_joints", r_joints);
    if(r_joints.compare("") != 0) {
        std::vector<double> r_joint_vals;
        str_to_vec(r_joints, r_joint_vals);
        if(r_joint_vals.size() == 7) {
            for(int i = 0; i < 7; i++) {
                r_arm_srv.request.positions[i] = r_joint_vals[i];
            }
        } else {
            ROS_ERROR("/poseRobot/r_joints must have seven values: %s", r_joints.c_str());
            return 1;
        }
    }
    bool move_r_arm = false;
    move_r_arm = node.param("/poseRobot/move_r_arm", move_r_arm);

    if(move_torso) {
        ros::ServiceClient torso_client = node.serviceClient<pr2_move::TorsoMove>("move_torso");
        ros::service::waitForService("move_torso");
        ROS_INFO("Moving torso to height of %f", torso_srv.request.goal);
        torso_client.call(torso_srv);
    }

    if(move_head) {
        ros::ServiceClient head_client = node.serviceClient<pr2_move::HeadMove>("move_head");
        ros::service::waitForService("move_head");
        std::string target_frame(head_srv.request.targetFrame);
        std::string reference_frame(head_srv.request.referenceFrame);

        if(target_frame.compare("")==0) {
            target_frame = "head_mount_kinect_depth_link";
        }

        if(reference_frame.compare("")==0) {
            reference_frame = "base_link";
        }
        ROS_INFO("Pointing %s at (%f, %f, %f) in %s", target_frame.c_str(),
                                                      head_srv.request.x,
                                                      head_srv.request.y,
                                                      head_srv.request.z,
                                                      reference_frame.c_str());
        head_client.call(head_srv);
    }

    if(move_l_gripper) {
        ros::ServiceClient l_gripper_client = node.serviceClient<pr2_move::GripperMove>("move_gripper");
        ros::service::waitForService("move_gripper");
        ROS_INFO("Setting left gripper width to %f", l_gripper_srv.request.position);
        l_gripper_client.call(l_gripper_srv);
    }

    if(move_r_gripper) {
        ros::ServiceClient r_gripper_client = node.serviceClient<pr2_move::GripperMove>("move_gripper");
        ros::service::waitForService("move_gripper");
        ROS_INFO("Setting right gripper width to %f", r_gripper_srv.request.position);
        r_gripper_client.call(r_gripper_srv);
    }

    if(move_l_arm) {
        ros::ServiceClient l_arm_client = node.serviceClient<pr2_move::ArmMoveJoints>("move_arm_joints");
        ros::service::waitForService("move_arm_joints");
        ROS_INFO("Setting left arm joints to (%f, %f, %f, %f, %f, %f, %f)", l_arm_srv.request.positions[0],
                                                                            l_arm_srv.request.positions[1],
                                                                            l_arm_srv.request.positions[2],
                                                                            l_arm_srv.request.positions[3],
                                                                            l_arm_srv.request.positions[4],
                                                                            l_arm_srv.request.positions[5],
                                                                            l_arm_srv.request.positions[6]);
        l_arm_client.call(l_arm_srv);
    }

    if(move_l_arm && move_r_arm) {
        // Make sure l arm has stopped moving before moving right arm
        ros::Duration d(1.0);
        d.sleep();
        while(Arm::armIsMoving("left"));
    }

    if(move_r_arm) {
        ros::ServiceClient r_arm_client = node.serviceClient<pr2_move::ArmMoveJoints>("move_arm_joints");
        ros::service::waitForService("move_arm_joints");
        ROS_INFO("Setting right arm joints to (%f, %f, %f, %f, %f, %f, %f)", r_arm_srv.request.positions[0],
                                                                             r_arm_srv.request.positions[1],
                                                                             r_arm_srv.request.positions[2],
                                                                             r_arm_srv.request.positions[3],
                                                                             r_arm_srv.request.positions[4],
                                                                             r_arm_srv.request.positions[5],
                                                                             r_arm_srv.request.positions[6]);
        r_arm_client.call(r_arm_srv);
    }



}
