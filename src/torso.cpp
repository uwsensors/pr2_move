#include <pr2_move/torso.h>

Torso::Torso() {
    torsoClient = new TorsoClient("torso_controller/position_joint_action", true);
    while(!torsoClient->waitForServer(ros::Duration(5.0))) {
        ROS_INFO("Waiting for the torso action server to come up");
    }
}
Torso::~Torso() {
    delete torsoClient;
}

void Torso::toHeight(double height) {
    pr2_controllers_msgs::SingleJointPositionGoal goal;
    height = std::max(0.0, std::min(0.195,height));
    goal.position = height;
    goal.min_duration = ros::Duration(2.0);
    goal.max_velocity = 1.0;

    ROS_INFO("Sending torso goal");
    torsoClient->sendGoal(goal);
    torsoClient->waitForResult(ros::Duration(8.0));
}




