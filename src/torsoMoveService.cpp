#include <pr2_move/torso.h>
#include <pr2_move/TorsoMove.h>
bool moveTorso(pr2_move::TorsoMove::Request &req, pr2_move::TorsoMove::Response & res) {

  Torso torso;
  torso.toHeight(req.goal);
  return true;

}

int main(int argc, char** argv) {

  ros::init(argc, argv, "move_torso_service");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("move_torso", moveTorso);
  ros::spin();

  return 0;

}
